all: release

HEADERS = $(wildcard mongoose-cpp/mongoose/*.h)
HEADERS_OUT = $(addprefix release/include/,$(notdir $(HEADERS)))

release: release/libmongoose.a release_headers

release_headers: $(HEADERS_OUT)
	mkdir -p release/include
	cp mongoose-cpp/mongoose.h release/mongoose.h

release/include/%.h: mongoose-cpp/mongoose/%.h
	mkdir -p release/include
	cp $< $@

release/libmongoose.a: build/libmongoose.a
	mkdir -p release
	cp build/libmongoose.a release/libmongoose.a

build/libmongoose.a: build/Makefile
	make -C build/

build/Makefile: mongoose-cpp/CMakeLists.txt
	mkdir -p build
	cd build && cmake ../mongoose-cpp

mongoose-cpp/CMakeLists.txt:
	git clone https://github.com/Gregwar/mongoose-cpp.git
